# Instalação do Julia+Vscode





#### 1. Baixar Julia no seguinte repositório:

> https://julialang.org/downloads/

- **Windows**: 

  - Escolha por 64-bit ou 32-bit e faça o download
  - Aperte no executável baixado e siga os procedimentos

- **Linux**: 

  - Escolha por: Generic Linux Binaries for x86

  - Mova o arquivo para o diretório home

  - Abra o terminal nessa pasta e utilize o comando:  `tar -xzf julia-1.4.2-linux-x86_64.tar.gz` para descompactar

  

#### 2. Baixar o Visual Code no site:

>  https://code.visualstudio.com/Download

- **Windows**:
  - Escolha por 64-bit ou 32-bit e faça o download
  - Aperte no executável baixado e siga os procedimentos
- **Linux** 
  - Escolha o .deb e faça o download
  - Abra o terminal no diretório do arquivo, faça `sudo dpkg -i nome_do_arquivo.deb` e siga os procedimentos

#### 3. Configurar Pacotes

- **Windows e Linux**:
  - Abra o Visual Studio Code e aperte `ctrl+shift+x` para acessar as extensões
  - Procure por "Julia" e instale o pacote `Julia`.
  - Reinicie o programa 

> 