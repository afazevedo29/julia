# Learning Julia

Este é um repositório de códigos em Julia, no intuito de ajudar no aprendizado da linguagem, focando principalmente, em resolver problemas de otimização inteira e combinatória.

- Na pasta `Aulas Particulares`  é encontrado materiais para o aprendizado da linguagem, que uso em minhas aulas particulares
- Na pasta `Exemplos(Códigos)`se encontra alguns exemplos utilizando a biblioteca JuMP, como uso de métodos como branch-and-cut, relaxação lagrangeana, entre outros.
- Na pasta ``Tutoriais`` é encontrado tutoriais de como instalar o software e o solver, dependendo do editor de texto. (Atom ou Vscode) 
- 

Se puder, dê uma estrela clicando no botão a direita `star` para ajudar o repositório :) 



